﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class EmployeesController : MyController
    {
        private technicalTestFikriEntities db = new technicalTestFikriEntities();

        // GET: Employees
        public ActionResult Index(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PositionSortParm = sortOrder == "position" ? "position_desc" : "position";
            ViewBag.OfficeSortParm = sortOrder == "office" ? "office_desc" : "office";
            ViewBag.AgeSortParm = sortOrder == "age" ? "age_desc" : "age";
            ViewBag.SalarySortParm = sortOrder == "salary" ? "salary_desc" : "salary";
            var employees = from s in db.Employees
                           select s;
            switch (sortOrder)
            {
                case "name_desc":
                    employees = employees.OrderByDescending(s => s.name);
                    break;
                case "position":
                    employees = employees.OrderBy(s => s.position);
                    break;
                case "position_desc":
                    employees = employees.OrderByDescending(s => s.position);
                    break;
                case "office":
                    employees = employees.OrderBy(s => s.office);
                    break;
                case "office_desc":
                    employees = employees.OrderByDescending(s => s.office);
                    break;
                case "age":
                    employees = employees.OrderBy(s => s.age);
                    break;
                case "age_desc":
                    employees = employees.OrderByDescending(s => s.age);
                    break;
                case "salary":
                    employees = employees.OrderBy(s => s.salary);
                    break;
                case "salary_desc":
                    employees = employees.OrderByDescending(s => s.salary);
                    break;

                default:
                    employees = employees.OrderBy(s => s.name);
                    break;
            }
            return View(employees.ToList());
        }

        public ActionResult ChangeLanguage(string lang, string url)
        {
            new LanguageMang().SetLanguage(lang);
            return RedirectToAction(url, "Employees");
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,position,office,age,salary")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,position,office,age,salary")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
